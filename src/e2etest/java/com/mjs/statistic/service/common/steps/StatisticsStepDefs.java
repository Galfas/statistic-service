package com.mjs.statistic.service.common.steps;

import com.jayway.restassured.response.Response;
import com.mjs.statistic.service.server.model.Summary;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.util.Map;

import static com.mjs.statistic.service.common.BaseApiClient.givenApiClientContJSON;
import static com.mjs.statistic.service.common.BaseApiClient.givenApiClientContUrlEconded;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class StatisticsStepDefs {

    Response response;

    Summary summary;

    @Before
    public void setup() throws InterruptedException {
        Thread.sleep(1000);
    }

    @When("^I request the summary$")
    public void i_request_the_summary() throws InterruptedException {
        response = null;
        Thread.sleep(110);

        response = givenApiClientContJSON()
                .get("/statistics");

        assertThat(response.statusCode(), equalTo(200));
        Map summaryAsMap = response.body().as(Map.class);

        summary = buildSummaryFrom(summaryAsMap);
    }

    @When("^I wait \"([^\"]*)\" millis to  request the summary$")
    public void i_request_the_summary_with_a_delay_of(Integer amount) throws InterruptedException {
        Thread.sleep(amount);
        i_request_the_summary();
    }

    @Given("^I register new Sales with value \"([^\"]*)\"$")
    public void i_register_new_sales_with_amount(Double amount) throws InterruptedException {
        i_register_sales_with_amount_millis_ago(amount, 0L);
    }

    @Given("^I a register new Sales with value \"([^\"]*)\" with delay \"([^\"]*)\" millis$")
    public void i_register_sales_with_amount_millis_ago(Double amount, Long delay) throws InterruptedException {
        Thread.sleep(delay);
        response = null;
        response = givenApiClientContUrlEconded()
                .param("amount", amount)
                .post("/sales");

        assertThat(response.statusCode(), equalTo(202));
    }

    @And("^with an average of \"([^\"]*)\"$")
    public void with_an_average_of_(Double avg) {
        assertEquals(avg, summary.getAverageAmountPerOrder());
    }

    @And("^with a total of \"([^\"]*)\"")
    public void with_a_count_of(Double count) {
        assertEquals(count, summary.getTotalSalesAmount());
    }

    private Summary buildSummaryFrom(Map<String, Object> summaryAsMap) {
        return new Summary((Double) summaryAsMap.get("total_sales_amount"),
                (Double) summaryAsMap.get("average_amount_per_order"));
    }
}
