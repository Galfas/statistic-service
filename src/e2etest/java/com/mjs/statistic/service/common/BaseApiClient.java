package com.mjs.statistic.service.common;

import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.config.DecoderConfig.decoderConfig;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import static com.jayway.restassured.config.RestAssuredConfig.newConfig;

public class BaseApiClient {

    public static RequestSpecification givenApiClientContJSON() {
        return buildApiClientWithCont(ContentType.JSON);
    }

    public static RequestSpecification givenApiClientContUrlEconded() {
        return buildApiClientWithCont(ContentType.URLENC);
    }

    private static RequestSpecification buildApiClientWithCont(ContentType contentType) {
        String baseUrl = "http://localhost:8080" + ServerConfig.serverContextPath;
        RestAssuredConfig config = newConfig()
                .encoderConfig(encoderConfig().defaultContentCharset("utf-8"))
                .decoderConfig(decoderConfig().defaultContentCharset("utf-8"));

        return given().contentType(contentType).port(ServerConfig.serverPort)
                .config(config)
                .baseUri(baseUrl)
                .log().all();
    }
}
