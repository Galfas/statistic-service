Feature: statistics retrieval

  Background:
    Given app has started
    And app is health

  Scenario: Return empty Summary without any sales added
    When I request the summary
    Then with an average of "0"
    And with a total of "0"

  Scenario: Return Summary of two sales
    Given I register new Sales with value "7000.40"
    And I register new Sales with value "10200"
    When I request the summary
    Then with an average of "8600.20"
    And with a total of "17200.4"

  Scenario: Return empty Summary after one sales got expired
    Given I register new Sales with value "7000.40"
    When I wait "501" millis to  request the summary
    Then with an average of "0.0"
    And with a total of "0.0"

  Scenario: Return Summary for 2 sales after one sales expire
    Given I register new Sales with value "7000.40"
    And I a register new Sales with value "10200" with delay "200" millis
    When I wait "350" millis to  request the summary
    Then with an average of "10200"
    And with a total of "10200"
