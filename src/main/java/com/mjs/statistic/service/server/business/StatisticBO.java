package com.mjs.statistic.service.server.business;

import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.Summary;

/**
 * Business object that contains the rules to handle Sales statistics.
 *
 * @author msaciloto
 */
public interface StatisticBO {

    /**
     * this method is responsible for receiving a sales object
     * insert it into repository.
     *
     * @param sales
     */
    void insert(Sales sales);

    /**
     * this method will retrieve an already processed summary.
     *
     * @return @Summary
     */
    Summary getSummary();

    /**
     * When called it updates the summary
     */
    void updateSummary();
}
