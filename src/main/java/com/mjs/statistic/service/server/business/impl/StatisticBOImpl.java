package com.mjs.statistic.service.server.business.impl;

import com.mjs.statistic.service.server.business.StatisticBO;
import com.mjs.statistic.service.server.conf.ServerConfiguration;
import com.mjs.statistic.service.server.dao.StatisticRepository;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.SalesConsolidate;
import com.mjs.statistic.service.server.model.Summary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.mjs.statistic.service.server.model.Summary.emptySummaryBuilder;

@Component
public class StatisticBOImpl implements StatisticBO {

    @Autowired
    private StatisticRepository statisticRepository;

    @Autowired
    private ServerConfiguration serverConfiguration;

    private Summary summary = emptySummaryBuilder();
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(Sales sales) {
        statisticRepository.insert(sales);
    }

    /**
     * {@inheritDoc}
     */
    public Summary getSummary() {
        return summary;
    }

    /**
     * this method is responsible for orchestrate the summary generation.
     */
    public synchronized void updateSummary() {
        statisticRepository.summariseSales();
        statisticRepository.removeExpiredSales(serverConfiguration.getMaxRangeTime());

        List<SalesConsolidate> salesConsolidateList = statisticRepository.fetch();
        this.summary = generateSummary(salesConsolidateList);
    }

    /**
     * from a range of valid sales this method is responsible to build Summary.
     *
     * @param salesConsolidateList List of sales consolidate that are valid for the current scenario.
     * @return current Summary based on the valid sales.
     */
    private Summary generateSummary(List<SalesConsolidate> salesConsolidateList) {
        if (salesConsolidateList.isEmpty())
            return emptySummaryBuilder();

        int size = salesConsolidateList.parallelStream().mapToInt(sale -> sale.getQuantity()).sum();
        Double total = salesConsolidateList.parallelStream().mapToDouble(sale -> sale.getAmount()).sum();
        Double avg = total / size;

        return new Summary(roundValue(total), roundValue(avg));
    }

    private Double roundValue(Double value) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(2, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }
}
