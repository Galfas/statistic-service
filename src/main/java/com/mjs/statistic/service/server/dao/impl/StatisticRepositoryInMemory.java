package com.mjs.statistic.service.server.dao.impl;

import com.mjs.statistic.service.server.dao.StatisticRepository;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.SalesConsolidate;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Component
public class StatisticRepositoryInMemory implements StatisticRepository {

    private ConcurrentLinkedQueue<Sales> salesQueue = new ConcurrentLinkedQueue<>();

    private ConcurrentLinkedQueue<SalesConsolidate> salesConsolidateQueue = new ConcurrentLinkedQueue<>();

    public StatisticRepositoryInMemory() {
    }

    public StatisticRepositoryInMemory(ConcurrentLinkedQueue<Sales> salesQueue,
                                       ConcurrentLinkedQueue<SalesConsolidate> salesConsolidateQueue) {
        this.salesQueue = salesQueue;
        this.salesConsolidateQueue = salesConsolidateQueue;
    }

    /**
     * {@inheritDoc}
     */
    public void insert(Sales sales) {
        salesQueue.add(sales);
    }

    /**
     * {@inheritDoc}
     */
    public List<SalesConsolidate> fetch() {
        SalesConsolidate[] salesArray = new SalesConsolidate[salesConsolidateQueue.size()];

        return Arrays.stream(this.salesConsolidateQueue.toArray(salesArray)).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    public void summariseSales() {
        if (!salesQueue.isEmpty()) {
            ConcurrentLinkedQueue<Sales> tempSalesQueue = this.salesQueue;
            this.salesQueue = new ConcurrentLinkedQueue<>();

            List<Sales> salesTemp = buildSalesArrayFrom(tempSalesQueue);

            Double sum = salesTemp.parallelStream().mapToDouble(sales -> sales.getAmount()).sum();

            salesConsolidateQueue.add(new SalesConsolidate(salesTemp.size(), sum));
        }
    }

    /**
     * {@inheritDoc}
     */
    public void removeExpiredSales(Integer delay) {
        Iterator it = this.salesConsolidateQueue.iterator();
        boolean keepRemoving = true;

        while (it.hasNext() && keepRemoving) {
            SalesConsolidate salesConsolidate = (SalesConsolidate) it.next();

            if (salesConsolidate.getTimestamp() + delay < Instant.now().toEpochMilli()) {
                this.salesConsolidateQueue.remove();
            } else {
                keepRemoving = false;
            }
        }
    }

    private List<Sales> buildSalesArrayFrom(ConcurrentLinkedQueue<Sales> tempSalesQueue) {
        Sales[] salesArray = new Sales[tempSalesQueue.size()];

        return Arrays.stream(tempSalesQueue.toArray(salesArray)).collect(Collectors.toList());
    }
}
