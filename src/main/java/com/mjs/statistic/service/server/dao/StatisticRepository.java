package com.mjs.statistic.service.server.dao;

import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.SalesConsolidate;

import java.util.List;

public interface StatisticRepository {

    /**
     * Method reponsible to insert a sales into the repository.
     *
     * @param sales
     */
    void insert(Sales sales);

    /**
     * Fetch all the current valid salesConsolidate.
     *
     * @return list of Sales Consolidate that has a timestamp in the valid interval.
     */
    List<SalesConsolidate> fetch();

    /**
     * Create a sales consolidate from the current sales.
     */
    void summariseSales();

    /**
     * Remove sales consolidate that are out of the valid interval
     *
     * @param delay value with the acceptable time in the past for a sale
     */
    void removeExpiredSales(Integer delay);
}
