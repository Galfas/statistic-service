package com.mjs.statistic.service.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Summary {

    @JsonProperty("total_sales_amount")
    private Double totalSalesAmount;

    @JsonProperty("average_amount_per_order")
    private Double averageAmountPerOrder;

    public Summary(Double totalSalesAmount, Double averageAmountPerOrder) {
        this.totalSalesAmount = totalSalesAmount;
        this.averageAmountPerOrder = averageAmountPerOrder;
    }

    public Double getTotalSalesAmount() {
        return totalSalesAmount;
    }

    public Double getAverageAmountPerOrder() {
        return averageAmountPerOrder;
    }

    public static Summary emptySummaryBuilder() {
        return new Summary(Double.valueOf(0), Double.valueOf(0));
    }
}
