package com.mjs.statistic.service.server.controller;

import com.mjs.statistic.service.server.business.StatisticBO;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.Summary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@RestController
public class StatisticController extends BaseController {

    public static final String AMOUNT_KEY = "amount";

    @Autowired
    StatisticBO statisticBO;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(path = "/sales", method = RequestMethod.POST, consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public void insert(@RequestParam Map<String, String> body) throws IOException {
        Sales sales = buildSalesFrom(body);

        statisticBO.insert(sales);
    }

    @RequestMapping(path = "/statistics")
    @ResponseStatus(HttpStatus.OK)
    public Summary fetchStatistics() throws IOException {
        return statisticBO.getSummary();
    }

    private Sales buildSalesFrom(@RequestParam Map<String, String> body) {
        if (!body.containsKey(AMOUNT_KEY)) {
            throw new IllegalArgumentException(format("key %s not fount in the request", AMOUNT_KEY));
        }

        return new Sales(Double.valueOf(body.get(AMOUNT_KEY)));
    }
}
