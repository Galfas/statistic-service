package com.mjs.statistic.service.server.model;

import java.time.Instant;

public class SalesConsolidate {

    private Long timestamp;

    private int quantity;

    private Double amount;

    public SalesConsolidate(int quantity, Double amount) {
        this.timestamp = Instant.now().toEpochMilli();
        this.quantity = quantity;
        this.amount = amount;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public int getQuantity() {
        return quantity;
    }

    public Double getAmount() {
        return amount;
    }
}
