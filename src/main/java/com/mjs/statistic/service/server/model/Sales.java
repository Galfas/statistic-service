package com.mjs.statistic.service.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class Sales {

    @NotNull
    @JsonProperty("sales_amount")
    private Double amount;

    public Sales(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }
}
