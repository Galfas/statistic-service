package com.mjs.statistic.service.server.scheduled;

import com.mjs.statistic.service.server.business.StatisticBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Schedule class responsible to run the updateSummary in background.
 */
@Component
class StatisticUpdateJob {

    @Autowired
    private StatisticBO statisticBO;

    @Scheduled(fixedDelayString = "${update.summary.job.delay}")
    public void run() {
        statisticBO.updateSummary();
    }
}