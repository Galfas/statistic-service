package com.mjs.statistic.service.impl;

import com.mjs.statistic.service.AbstractIntegrationTest;
import com.mjs.statistic.service.server.model.Summary;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StatisticsIntegrationTest extends AbstractIntegrationTest {

    @Test
    public void shouldRetrieveEmptySummary() throws Exception {
        Summary expectedSummary = Summary.emptySummaryBuilder();

        MvcResult mvcResult = mockMvc.perform(get(
                "/statistics")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        Map summaryAsMap = mapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Map.class);
        assertSummaryAsMap(expectedSummary, summaryAsMap);
    }

    @Test
    public void shouldReceiveBadRequestSalesWithoutAmount() throws Exception {
        mockMvc.perform(post(
                "/sales")
                .param("value", "100")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldRetrieveSummaryBasedOnTwoSales() throws Exception {
        Summary expectedSummary =
                new Summary(Double.valueOf(140), Double.valueOf(70));

        mockMvc.perform(post(
                "/sales")
                .param("amount", "100")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isAccepted()).andReturn();

        mockMvc.perform(post(
                "/sales")
                .param("amount", "40")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                .andExpect(status().isAccepted()).andReturn();

        Thread.sleep(120);

        MvcResult mvcResult = mockMvc.perform(get(
                "/statistics")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        Map summaryAsMap = mapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Map.class);
        assertSummaryAsMap(expectedSummary, summaryAsMap);
    }

    private void assertSummaryAsMap(Summary expectedSummary, Map summaryAsMap) {
        Assert.assertEquals(expectedSummary.getAverageAmountPerOrder(), summaryAsMap.get("average_amount_per_order"));
        Assert.assertEquals(expectedSummary.getTotalSalesAmount(), summaryAsMap.get("total_sales_amount"));
    }
}
