package com.mjs.statistic.service.server.controller;

import com.mjs.statistic.service.server.business.StatisticBO;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.Summary;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticsControllerTest {

    @Mock
    StatisticBO statisticBO;

    @InjectMocks
    StatisticController statisticController = new StatisticController();

    @Test
    public void shouldRetrieveSummary() throws IOException {
        Summary expectedSummary = Summary.emptySummaryBuilder();
        Mockito.when(statisticBO.getSummary()).thenReturn(expectedSummary);
        Summary summary = statisticController.fetchStatistics();

        Assert.assertNotNull(summary);
        assertEquals(expectedSummary, summary);
    }

    @Test
    public void shouldCallInsertWhenMethodIsCalled() throws IOException {
        Map<String, String> salesAsMap = salesAsMapBuilder();

        statisticController.insert(salesAsMap);
        verify(statisticBO, times(1)).insert(Mockito.any(Sales.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldPassTheExceptionReceived() throws IOException {
        Map<String, String> sales = salesAsMapBuilder();
        doThrow(new IllegalArgumentException()).when(statisticBO).insert(Mockito.any(Sales.class));
        statisticController.insert(sales);
    }

    private Map<String, String> salesAsMapBuilder() {
        Map salesAsMap = new HashMap<String, String>();
        salesAsMap.put("amount", "100");

        return salesAsMap;
    }
}
