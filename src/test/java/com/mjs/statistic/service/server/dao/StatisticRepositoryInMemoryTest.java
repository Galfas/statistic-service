package com.mjs.statistic.service.server.dao;

import com.mjs.statistic.service.server.dao.impl.StatisticRepositoryInMemory;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.SalesConsolidate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticRepositoryInMemoryTest {

    private ConcurrentLinkedQueue<Sales> salesQueue = new ConcurrentLinkedQueue<>();

    private ConcurrentLinkedQueue<SalesConsolidate> salesConsolidateQueue = new ConcurrentLinkedQueue<>();

    private StatisticRepositoryInMemory statisticRepository =
            new StatisticRepositoryInMemory(salesQueue, salesConsolidateQueue);

    public void setup() {
        salesQueue.clear();
        salesConsolidateQueue.clear();
    }

    @Test
    public void shouldAddSalesToSalesQueue() throws IOException {
        Sales expectedSales = new Sales(Double.valueOf(100));
        statisticRepository.insert(expectedSales);

        Assert.assertEquals(1, salesQueue.size());
        Assert.assertEquals(expectedSales, salesQueue.peek());
        Assert.assertEquals(0, salesConsolidateQueue.size());
    }

    @Test
    public void shouldFetchFromSalesConsolidate() throws IOException {
        SalesConsolidate expectedSalesConsolidate = new SalesConsolidate(10, Double.valueOf(100));
        salesConsolidateQueue.add(expectedSalesConsolidate);

        List<SalesConsolidate> salesConsolidates = statisticRepository.fetch();
        Assert.assertEquals(1, salesConsolidates.size());
        Assert.assertEquals(expectedSalesConsolidate, salesConsolidates.get(0));
        Assert.assertEquals(0, salesQueue.size());
    }

    public void shouldNotGenerateConsolidateForEmptySales() {
        statisticRepository.summariseSales();

        Assert.assertEquals(salesConsolidateQueue.size(), 0);
    }

    public void shouldSummarizeTwoSales() {
        Sales expectedSales = new Sales(Double.valueOf(200));
        Sales expectedSales2 = new Sales(Double.valueOf(200));
        salesQueue.add(expectedSales);
        salesQueue.add(expectedSales2);

        statisticRepository.summariseSales();

        Assert.assertEquals(1, salesConsolidateQueue.size());
        SalesConsolidate salesConsolidate = salesConsolidateQueue.poll();
        Assert.assertEquals(2, salesConsolidate.getQuantity());
        Assert.assertEquals(Double.valueOf(400), salesConsolidate.getAmount());
    }

    @Test
    public void shouldReturnConsolidateEmptyAfterAllConsolidateExpires() throws IOException {

        SalesConsolidate salesConsolidate = new SalesConsolidate(1, Double.valueOf(100));
        SalesConsolidate salesConsolidate2 = new SalesConsolidate(1, Double.valueOf(200));
        salesConsolidateQueue.add(salesConsolidate);
        salesConsolidateQueue.add(salesConsolidate2);

        statisticRepository.removeExpiredSales(-1);

        Assert.assertEquals(salesConsolidateQueue.size(), 0);
    }

    @Test
    public void shouldReturnOnlyOneConsolidateAfterFirstExpires() throws InterruptedException {

        SalesConsolidate salesConsolidate = new SalesConsolidate(1, Double.valueOf(100));
        Thread.sleep(50);
        SalesConsolidate salesConsolidate2 = new SalesConsolidate(20, Double.valueOf(200));
        salesConsolidateQueue.add(salesConsolidate);
        salesConsolidateQueue.add(salesConsolidate2);

        statisticRepository.removeExpiredSales(40);

        Assert.assertEquals(salesConsolidateQueue.size(), 1);
        SalesConsolidate salesConsolidateResult = salesConsolidateQueue.poll();
        Assert.assertEquals(salesConsolidateResult.getAmount(), Double.valueOf(200));
        Assert.assertEquals(salesConsolidateResult.getQuantity(), 20);
    }

    @Test
    public void shouldReturnTwoConsolidatesBeforeExpires() throws InterruptedException {

        SalesConsolidate salesConsolidate = new SalesConsolidate(80, Double.valueOf(800));
        SalesConsolidate salesConsolidate2 = new SalesConsolidate(20, Double.valueOf(200));
        salesConsolidateQueue.add(salesConsolidate);
        salesConsolidateQueue.add(salesConsolidate2);

        statisticRepository.removeExpiredSales(100);

        Assert.assertEquals(salesConsolidateQueue.size(), 2);

        SalesConsolidate salesConsolidateResult = salesConsolidateQueue.poll();
        Assert.assertEquals(Double.valueOf(800), salesConsolidateResult.getAmount());
        Assert.assertEquals(80, salesConsolidateResult.getQuantity());

        SalesConsolidate salesConsolidateResult2 = salesConsolidateQueue.poll();
        Assert.assertEquals(Double.valueOf(200), salesConsolidateResult2.getAmount());
        Assert.assertEquals(20, salesConsolidateResult2.getQuantity());
    }

    private Sales salesBuilder(Double amount) {
        return new Sales(amount);
    }
}
