package com.mjs.statistic.service.server.business;

import com.mjs.statistic.service.server.business.impl.StatisticBOImpl;
import com.mjs.statistic.service.server.conf.ServerConfiguration;
import com.mjs.statistic.service.server.dao.StatisticRepository;
import com.mjs.statistic.service.server.model.Sales;
import com.mjs.statistic.service.server.model.SalesConsolidate;
import com.mjs.statistic.service.server.model.Summary;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticBOImplTest {

    @Mock
    private StatisticRepository statisticRepository;

    @Mock
    private ServerConfiguration serverConfiguration;

    @InjectMocks
    StatisticBO statisticBO = new StatisticBOImpl();

    @Test
    public void shouldInsertSalesIntoRepository() {

        Mockito.when(serverConfiguration.getMaxRangeTime()).thenReturn(60000);
        Sales sales = salesBuilder(Double.valueOf(0));

        statisticBO.insert(sales);
        verify(statisticRepository, times(1)).insert(sales);
    }

    @Test
    public void shouldReturnEmptySummaryIfNoSalesIsAdded() {
        Summary summary = statisticBO.getSummary();

        Assert.assertEquals(Double.valueOf(0), summary.getTotalSalesAmount());
        Assert.assertEquals(Double.valueOf(0), summary.getAverageAmountPerOrder());
    }

    @Test
    public void shouldBuildSummaryWithNoConsolidate() {
        Mockito.when(serverConfiguration.getMaxRangeTime()).thenReturn(10);
        Mockito.when(statisticRepository.fetch()).thenReturn(new ArrayList<>());

        statisticBO.updateSummary();

        Summary summary = statisticBO.getSummary();

        Assert.assertEquals(Double.valueOf(0), summary.getTotalSalesAmount());
        Assert.assertEquals(Double.valueOf(0), summary.getAverageAmountPerOrder());

        verify(statisticRepository, times(1)).summariseSales();
        verify(statisticRepository, times(1)).removeExpiredSales(10);
    }

    @Test
    public void shouldBuildSummaryWithTwoConsolidate() {
        SalesConsolidate salesConsolidate = new SalesConsolidate(10, Double.valueOf(10));
        SalesConsolidate salesConsolidate2 = new SalesConsolidate(20, Double.valueOf(20));
        List<SalesConsolidate> salesConsolidateList = Arrays.asList(salesConsolidate, salesConsolidate2);

        Mockito.when(serverConfiguration.getMaxRangeTime()).thenReturn(10);
        Mockito.when(statisticRepository.fetch()).thenReturn(salesConsolidateList);

        statisticBO.updateSummary();

        Summary summary = statisticBO.getSummary();

        Assert.assertEquals(Double.valueOf(30), summary.getTotalSalesAmount());
        Assert.assertEquals(Double.valueOf(1), summary.getAverageAmountPerOrder());

        verify(statisticRepository, times(1)).summariseSales();
        verify(statisticRepository, times(1)).removeExpiredSales(10);
    }

    private Sales salesBuilder(Double amount) {
        return new Sales(amount);
    }

    public void assertSummary(Summary expectedSummary, Summary resultSummary) {

        assertEquals(expectedSummary.getAverageAmountPerOrder(), resultSummary.getAverageAmountPerOrder());
        assertEquals(expectedSummary.getTotalSalesAmount(), resultSummary.getTotalSalesAmount());
    }
}
